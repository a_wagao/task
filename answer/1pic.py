#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2

img1 = cv2.imread('lena.png') #元画像の読み込み
img2 = cv2.imread('lena.png',0) #画像をグレースケール化する処理

cv2.imshow("color_image",img1) #画像の表示　cv2.imshow("ウィンドウ名",表示する画像)
cv2.imshow("gray_image",img2)
cv2.imwrite("gray_image.png",img2)
cv2.waitKey(0) #キー入力を待つ
cv2.destroyAllWindows() #ウィンドウの破棄