#!/usr/bin/env python
# -*- coding: utf-8 -*-

#テンプレートマッチング

import cv2
import numpy as np

img = cv2.imread('physicist.jpg',0) #探索する画像
temp = cv2.imread('einstein.png',0) #テンプレート画像 両方グレースケール化

res = cv2.matchTemplate(img,temp,cv2.TM_CCOEFF_NORMED) #cv2.matchTemplates(探索する画像，テンプレート，比較方法)
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
top_left = max_loc
w, h = temp.shape[::-1]
bottom_right = (top_left[0] + w, top_left[1] + h)

res = cv2.imread('physicist.jpg')
cv2.rectangle(res,top_left,bottom_right,(255,0,0),2)
cv2.imwrite('res.png', res)
cv2.imshow('res', res)
cv2.waitKey(0)
cv2.destroyAllWindows()