#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np

face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml') #()内にカスケードのパス

cap = cv2.VideoCapture(0)


while(1):
    _, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', frame)
    cv2.imshow('gray', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    faces = face_cascade.detectMultiScale(frame) #顔の検出，返り値(左上のx座標，y座標，幅，高さ)

    for x, y, w, h in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2) #領域の枠を描く
        face = frame[y: y+h, x: x+w]
        face_gray = gray[y: y+h, x: x+w]
    
    cv2.imshow('detected',face)


cv2.destroyAllWindows()