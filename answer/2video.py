#!/usr/bin/env python
# -*- coding: utf-8 -*-

#カメラから映像を取得し表示する 
#取得した映像をグレースケール化しウィンドウに表示する

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(1):
    _, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', frame)
    cv2.imshow('gray', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cv2.destroyAllWindows()