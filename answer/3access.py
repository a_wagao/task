#!/usr/bin/env python
# -*- coding: utf-8 -*-

#画素値を読み込んでみよう．

import numpy as np
import sys
import cv2

def main():
    img = cv2.imread("lena.png")
    height, width, channel = img.shape #shapeで幅と高さをタプルで取得できる
    size = width * height
    #Type = img.dtype

    print('高さ:{0},幅:{1},チャンネル数:{2}, 画素数{3}\n'.format(height, width, channel, size))
    print 'B:\n', img[0]
    print 'G:\n', img[1]
    print 'R:\n', img[2]

if __name__ == '__main__':
    main()