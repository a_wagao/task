#!/usr/bin/env python
# -*- coding: utf-8 -*-

#ガウシアンとフィルターのみでの2値化の比較

import pylab as plt
plt.rcParams['font.family'] = 'IPAexGothic'
plt.rcParams['font.size'] = 12

import cv2
import numpy as np

img = cv2.imread("lena.png")
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

th1 = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

blur = cv2.bilateralFilter(img_gray, 6, 12, 3)
ret, th2 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
ret, th3 = cv2.threshold(blur, 70, 255, cv2.THRESH_BINARY_INV)

print ret
plt.subplot(2,2,1), plt.imshow(img, 'gray')
plt.title('imput image')
plt.subplot(2,2,2), plt.imshow(th1, 'gray')
plt.title('Adaptive Gaussian Thresholdimg')
plt.subplot(2,2,3), plt.imshow(th3, 'gray')
plt.title(u"フィルターのみ")

plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()