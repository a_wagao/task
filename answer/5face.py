#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cv2
import numpy as np

face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml') #()内にカスケードのパス

img = cv2.imread('lena.png')
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(img_gray) #顔の検出，返り値(左上のx座標，y座標，幅，高さ)

for x, y, w, h in faces:
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2) #領域の枠を描く
    face = img[y: y+h, x: x+w]
    face_gray = img_gray[y: y+h, x: x+w]

cv2.imshow('face', img)
cv2.imwrite('face_detected.jpg', img)

cv2.waitKey(0)
cv2.destroyAllWindows()