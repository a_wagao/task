#!/usr/bin/env python
# -*- coding: utf-8 -*-

#カメラから映像を取得し特定の色のみ取得し表示する

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(1):
    _, frame = cap.read() #ビデオの読み込み
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #BGRからHSVに変換(色相H,色彩S,明度V)
    
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
    
    lower_red = np.array([0,0,130])
    upper_red = np.array([120,120,255])

    mask_blue = cv2.inRange(hsv, lower_blue, upper_blue) #青色の画像のみ取得する
    res_blue = cv2.bitwise_and(frame, frame, mask = mask_blue)

    mask_red = cv2.inRange(hsv, lower_red, upper_red)
    res_red = cv2.bitwise_and(frame, frame, mask = mask_red)


    cv2.imshow('frame', frame)
    cv2.imshow('mask_blue', mask_blue)
    cv2.imshow('res_blue', res_blue)
    cv2.imshow('mask_red', mask_red)
    cv2.imshow('res_red', res_red) #赤色だけどうまくいかない
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()


