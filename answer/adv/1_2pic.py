#!/usr/bin/env python
# -*- coding: utf-8-*-

#画像から一部を切り出す

import numpy as np
import cv2

def main():
    img = cv2.imread("black.png")

    x, y = 40, 40 #切り取り画像(窓画像)の左上の座標
    w, h = 60, 60

    part = img[x:x+w, y:y+h]

    cv2.imwrite("black_slice.png", part)

if __name__ == "__main__":
    main()