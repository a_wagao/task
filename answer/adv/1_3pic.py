#!/usr/bin/env python
# -*- coding: utf-8 -*-

#黒以外の箇所のみ切り取りもう1枚の画像を背景にする

import cv2
import numpy as np

img_src = cv2.imread("", 1) #背景にさせたい画像
img_mask = cv2.imread("", 0)　#黒以外を切り抜きたい画像

img2 = cv2.bitwise_and(img_src, img_src, mask=img_mask)
cv2.imwrite("mask_result.png", img2)