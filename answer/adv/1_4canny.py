#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2

img_src = cv2.imread('books.jpg', 1)
img_gray = cv2.cvtColor(img_src, cv2.COLOR_BGR2GRAY)
img_dst = cv2.Canny(img_gray, 350, 800)

cv2.imshow('src', img_src)
cv2.imshow('dst', img_dst)
cv2.waitKey(0)
cv2.destroyAllWindows()